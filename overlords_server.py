#!/usr/bin/python
"""
A RESTful server which logs commit messages from git repositories.

Based on flask and json.

The collected commits are persisted using rotating log files; This:
	* Prevents unbounded data growth
	* Allows online and off-line analysis of the  commits

For each commit, one line is logged containing a json representation
of the required commit data, containing an internal timestamp

A plugin API is provided, such that all classes deriving from
OverlordsHandlerBase are automatically registed for event notification.
"""

import traceback
import os

from flask import Flask, request, json

from plugins.PluginContainer import PluginContainer

if __name__ == '__main__':
	try:
		app      = Flask(__name__)
		handlers = []
		
		PluginContainer.load_plugins()
		
		#Initialize all loaded plugins:
		for cls in PluginContainer.plugins.values():
			new_handler = cls(app)
			new_handler.setup()
			handlers.append(new_handler)
		
		@app.route('/')
		def api_root():
				return "Welcome to the Overloards' Realm!\n"
		
		
		@app.route('/commit-log', methods = ['POST'])
		def commit_log():
			if request.headers['Content-Type'] != 'application/json':
				return "415 Unsupported Media Type - I like only application/json ;)\n", 415
			
			#Execute handlers from all loaded plugins:
			for h in handlers:
				h.on_handle_commit_log(request.json)
			
			return "OK"
		
		app.run(debug=True, use_reloader=False)
	except KeyboardInterrupt:
		os._exit(-1)		#abruptly terminate all threads
		raise
	except Exception as e:
		traceback.print_exc()
		os._exit(-1)		#abruptly terminate all threads
		raise
		
		
