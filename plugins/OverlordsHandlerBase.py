#!/usr/bin/python


from plugins.PluginContainer import PluginContainer

class OverlordsHandlerBase:
	"""
	Base class for plugins.
	A deriving class will be automatically registered with our plugin container.
	
	After implementing the correct on_handle methods, deriving classes
	will be notified of requests recieved by our flask application.
	"""
	def __init__(self, app):
		self.app = app
		
	
	def setup(self):
		pass
		
	def on_handle_commit_log(self, commit_dict):
		self.app.logger.debug('on_handle_commit_log')
		pass
	
	class __metaclass__(type):
		"""
		A deriving class will be reqistered with the plugin container
		automatically uppon loading through this custom init.
		"""
		def __init__(cls, name, base, attrs):
			if not hasattr(cls, 'my_container'):
				#print "NEW"
				cls.my_container = PluginContainer
			
			cls.my_container.register(cls)
		

