#!/usr/bin/python
"""
A handler implementation providing persistance of commit data through
the rotating log files, as described elsewhere.
"""


import logging
import logging.handlers
from flask import json

from plugins.OverlordsHandlerBase import OverlordsHandlerBase



#TODO: externalize configuration
LOG_FILENAME     = 'overlords_commits.log'
LOG_MAX_BYTES    = 1024*1024
LOG_BACKUP_COUNT = 20


class OverlordsRecordingHandler(OverlordsHandlerBase):
	"""
	Driven by our flask application, handles requests.
	"""
	def __init__(self, app):
		self.app = app
	
	def setup(self):
		self.plogger = self.create_default_persistance_logger()
		
		
	def on_handle_commit_log(self, commit_dict):
		"""
		Record a new commit by simply dumping it into our persistance logger.
		"""
		self.app.logger.debug("on_handle_commit_log: %s"%repr(commit_dict.keys()))
		
		#This is our version of persistance:
		self.plogger.info(json.dumps(commit_dict))
		return

	@staticmethod
	def create_default_persistance_logger():
		"""
		Use hardcoded parameters to construct our persistance logger
		:returns: a logger with a rotating file handler 
		"""
		my_logger = logging.getLogger('commit_log_persistance')
		my_logger.setLevel(logging.DEBUG)
		handler = logging.handlers.RotatingFileHandler(LOG_FILENAME, maxBytes=LOG_MAX_BYTES, backupCount=LOG_BACKUP_COUNT)
		
		my_logger.addHandler(handler)
		
		return my_logger




