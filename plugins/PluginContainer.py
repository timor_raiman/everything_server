#!/usr/bin/python
import imp, pkgutil

PLUGINS_DIRS      = ['./plugins']

#TODO: use the logging sytem (today, it's not initialized when we are here)

class PluginContainer:
	"""
	A container for loading and registering our plugins.
	"""
	plugins = {}
	
	@classmethod
	def register(cls, plugin):
		if plugin.__name__ not in cls.plugins:
			cls.plugins[plugin.__name__]=plugin
			print "Registered %s" % str(plugin)
		#print PluginContainer.plugins
	
	
	@classmethod
	def load_plugins(cls):
		#uncomment to remove OverlordsHandlerBase from plugins
		#cls.plugins = []
		for _, name, _ in pkgutil.iter_modules(PLUGINS_DIRS):
			fid, pathname, desc = imp.find_module(name, PLUGINS_DIRS)
			try:
				imp.load_module(name, fid, pathname, desc)
			except Exception as e:
				print "Failed to load module '%s': %s" % (pathname, str(e))
			if fid:
				fid.close()

